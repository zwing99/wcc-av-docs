# Hooking Up An Acoustic Guitar

Some helpful tips on hooking up an acoustic guitar

## Standard Unbalanced Hookup

### Diagram

```mermaid
graph TB;
    AG([Acoustic Guitar])--Unbalanced\nInstrument Cable-->T[Tuner];
    T--Unbalanced\nInstrument Cable-->DI[Direct Box];
    DI--Balanced\nXLR Cable--> FP1([Floor Pocket Input])
```

### Items Reference

- [Direct Box][di]
- [Unbalanced Instrument Cable][ic]
- [Balanced (XLR) Cable][xlr]

## Balanced FX Pedal (like HX Stomp)

### Diagram

```mermaid
graph TB;
    AG([Acoustic Guitar])--Unbalanced\nInstrument Cable-->T[Balanced FX Pedal];
    T--Balanced Male TRS to\nBalanced Male XLR Cable--> FP1([Floor Pocket Input L]) & FP2([Floor Pocket Input R]);
```

### Items Reference

- [Unbalanced Instrument Cable][ic]
- [Balanced Male TRS to Balanced Male XLR][trs]

[ic]: ../03-audio/stage/cable-types.md#instrument-cable
[xlr]: ../03-audio/stage/cable-types.md#xlr
[trs]: ../03-audio/stage/cable-types.md#balanced-male-trs-to-male-xlr
[di]: ../03-audio/stage/direct-boxes.md
