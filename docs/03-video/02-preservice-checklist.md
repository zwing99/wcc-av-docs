---
tags: ["checklist"]
---

# Preservice Checklist

Things to be sure are done in the 10 mins before service starts

## Propresenter

- [ ] Music is playing
- [ ] Announcement reel is going

## Lights

- [ ] Make sure lights are set to "pre service" scene

## Livestream

- [ ] Start Aja streaming device
- [ ] Start Live Stream on youtube
