# Direct Boxes (DIs)

## Example

**Here is an example of standard passive direct box:**

![Direct Boxes](./img/direct-box.jpg)

## Connections

### Input

DIs have [1/4" Female TS](./cable-types.md#ts) **Input** that is used to connect instruments like acoustic guitars or electric pianos with an [instrument cable](./cable-types.md#instrument-cable).

### Thru

_Some_ DIs have [1/4" Female TS](./cable-types.md#ts) **Thru** that is used to relay the [input](#input) signal to a guitar amp or on stage monitor. We _rarely_ use these.

### XLR

DIs have [Male XLR](./cable-types.md#xlr) **Out** that is used to connect the DI to a snake input with an [xlr cable](./cable-types.md#xlr-cable-balanced).

## Buttons

### Ground Lift

Should typically be off unless you are getting a ground loop hum. In which case flip the switch to hopefully remove the hum.

### Pad

Used to pad (or reduce the gain) of the input. **Should be "off"** unless input signal is too strong for sound board.

## Types

### Passive vs Active

**Active** DIs require [+48V of phantom power](../soundboard/input-processing.md#1-phantom-power-48v) to operate. The phantom power must be turned on at the sound board for the channel it is connected to. On Behringer brand and many other sound boards, the phantom power button is labeled "**+48V** on the channel strip." **Passive** DIs can be used without phantom power.

### Mono vs Stereo

Standard **Mono** DIs have one [TS inputs](#input) and one [xlr outputs](#xlr). **Stereo** DIs have two [TS inputs](#input) and two [xlr outputs](#xlr).
