# Cable Types

[See here](https://www.cablematters.com/Blog/Audio/audio-cable-types-a-complete-guide) for an in-depth guide to cables.

## Common Cable Connectors

:::note
All cable connectors come in Male and Female variants
:::
:::note
TS and TRS cables can come in in 1/4" and 1/8"(3.5mm) variants
:::

### XLR

![XLR](./img/xlr-connector.jpeg)

### TS

**TS = Tip Sleeve**

![TS](./img/ts-connector.jpeg)

### TRS

**TRS = Tip Ring Sleeve**

![TRS](./img/trs-connector.jpeg)

## Common Cables

### XLR Cable (Balanced)

[Male XLR](#xlr) to [Female XLR](#xlr) cable that is commonly used to connected balanced sources like [Direct Boxes][dis] or [Microphones](./microphones.md) to snake inputs which take them to Front of House.

### Instrument Cable

[1/4" Male TS](#ts) to [1/4" Male TS](#ts) cable with that is commononly used to connected instruments (acoutic guitars, electric guitars, electric pianos, mandolins, etc) sources to amplifiers, guitar pedals, or [Direct Boxes][dis].

## Other Cables

### Balanced Male TRS to Male XLR

A [1/4 Male TRS](#trs) to [Male XLR](#xlr) cable used to hook up balanced output sources to XLR snake inputs directly **without** the need for a [Direct Box][dis]. One example of a pedal this can be used with is the balanced outputs on a [Line6 HX Stomp](https://www.sweetwater.com/store/detail/HXStomp--line-6-hx-stomp-guitar-multi-effects-floor-processor).

:::caution
**DO NOT** use these cables with unbalanced outputs on guitar pedals, acoustic guitars, etc.
:::

### InEar Headphone Cable

We use [Elite Core Belt Packs](https://a.co/d/hSZontX) that take a special cable: [1/4" TRS Male](#trs) to [XLR Male](#xlr)

![Belt Pack](./img/belt-pack.jpg)

[dis]: ./direct-boxes.md
