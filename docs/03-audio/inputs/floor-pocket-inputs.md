# Floor Pockets Inputs

Some common stage floor pocket hookup plans.

## Back Right (Drum) Pocket

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|     1 | Kick       | Cajon          |
|     2 | Snare Top  | _~_            |
|     3 | Snare Btm  | _~_            |
|     4 | High Tom   | _~_            |
|     5 | Low Tom    | _~_            |
|     6 | High Hat   | _~_            |
|     7 | OH L       | OH for Cajon   |
|     8 | OH R       | _~_            |
|     9 | Bass       | _~_            |
|    10 | _~_        | _~_            |

## Back Left (Guitar) Pocket

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|    11 | Guitar L   | Piano L        |
|    12 | Guitar R   | Piano R        |
|    13 | Click      | Piano Click    |
|    14 | _~_        | _~_            |
|    15 | Vox        | Vox            |
|    16 | _~_        | _~_            |
|    17 | _~_        | _~_            |
|    18 | _~_        | _~_            |
|    19 | _~_        | _~_            |
|    20 | _~_        | _~_            |

## Front Far Left (Piano) Pocket

| input | instrument  | alt/instrument |
| ----: | ----------- | -------------- |
|    21 | Piano L     | Guitar L       |
|    22 | Piano R     | Guitar R       |
|    23 | Piano Click | _~_            |
|    24 | Piano Extra | _~_            |
|    25 | Vox         | Vox            |
|    26 | _~_         | _~_            |

## Front Center Left (Female) Pocket

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|    27 | Vox        | _~_            |
|    28 | _~_        | _~_            |
|    29 | Guitar L   | _~_            |
|    30 | Guitar R   | _~_            |
|    31 | _~_        | Vox            |
|    32 | _~_        | _~_            |

## Front Center Right (Male) Pocket

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|    34 | Guitar L   | _~_            |
|    35 | Guitar R   | _~_            |
|    36 | Vox        | _~_            |
|    37 | _~_        | _~_            |

## Front Far Right (Extra) Pocket

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|    37 | _~_        | _~_            |
|    38 | _~_        | _~_            |
|    39 | _~_        | _~_            |
|    40 | _~_        | _~_            |
|    41 | _~_        | _~_            |
|    42 | _~_        | _~_            |

## Rack

| input | instrument | alt/instrument |
| ----: | ---------- | -------------- |
|    43 | Handheld   | _~_            |
|    44 | Pastor     | _~_            |
|    45 | _~_        | _~_            |
|    46 | _~_        | _~_            |
|    47 | _~_        | _~_            |
|    48 | _~_        | _~_            |
