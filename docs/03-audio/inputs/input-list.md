# Input List

## SQ inputs

| SQ Input | Instrument                             | DCAs                |
| -------: | :------------------------------------- | ------------------- |
|        1 | Kick                                   | drum, band          |
|        2 | Snare Top                              | drum, snr/cjn, band |
|        3 | Snare Btm                              | drum, snr/cjn, band |
|        4 | High Tom                               | drum, band          |
|        5 | Low Tom                                | drum, band          |
|        6 | High Hat                               | drum, band          |
|        7 | OH L                                   | drum, band          |
|        8 | OH R                                   | drum, band          |
|     9/10 | Aux Per L/R                            | drum, band          |
|       11 | Cajon                                  | drum, snr/cjn, band |
|       12 | Click (NOT IN MAINS)                   |                     |
|    13/14 | Bass L/R                               | band                |
|    15/16 | AG L/R                                 | inst, band          |
|    17/18 | EG L/R                                 | inst, band          |
|    19/20 | Extra Guitar L/R                       | inst, band          |
|    21/22 | Keys L/R                               | inst, band          |
|       23 | Piano Vox                              | vox, band           |
|       24 | Female Vox                             | vox, band           |
|       25 | Male Vox                               | vox, band           |
|       26 | Extra Vox                              | vox, band           |
|       27 | Handheld                               | spoken              |
|       28 | Pastor Mic                             | spoken              |
|    29/30 | Bluetooth L/R                          |                     |
|    31/32 | Mac L/R                                |                     |
|       33 | _~_                                    |                     |
|       34 | _~_                                    |                     |
|       35 | _~_                                    |                     |
|       36 | _~_                                    |                     |
|       37 | _~_                                    |                     |
|       38 | TapCom (used for livestream tap tempo) |                     |
|    39/40 | LiveStream L/R (NOT IN MAINS)          |                     |
|       41 | Crowd L (NOT IN MAINS)                 |                     |
|       42 | Crowd R (NOT IN MAINS)                 |                     |
|       43 | _~_                                    |                     |
|       44 | _~_                                    |                     |
|       45 | _~_                                    |                     |
|       46 | _~_                                    |                     |
|       47 | _~_                                    |                     |
|       48 | _~_                                    |                     |
