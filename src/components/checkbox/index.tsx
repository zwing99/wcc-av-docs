import React, { useId } from "react";

import { useSessionStorageState, useLocalStorageState } from "ahooks";

export type CBProps = {
  children: React.ReactNode;
  s: number; // seed for hash
  indent: string; // seed for hash
};

function hashCode(s: string, seed: number = 0): string {
  var hash = seed;
  for (var i = 0; i < s.length; i++) {
    var code = s.charCodeAt(i);
    hash = (hash << 5) - hash + code;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash.toString();
}

export const CB: React.FC<CBProps> = ({ children, s = 0, indent = "0px" }) => {
  const [isChecked, setIsChecked] = useSessionStorageState<boolean>(
    hashCode(children.toString(), s),
    {
      defaultValue: false,
    },
  );

  const handleChange = (event) => {
    setIsChecked(!isChecked);
  };

  return (
    <div style={{ marginLeft: indent }}>
      <input
        type="checkbox"
        checked={isChecked}
        onClick={handleChange}
        onChange={(e) => {}}
      />
      {children}
    </div>
  );
};
