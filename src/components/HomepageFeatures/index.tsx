import Link from "@docusaurus/Link";
import { useColorMode } from "@docusaurus/theme-common";

import React from "react";
import { ArrowRightCircle } from "react-feather";

import clsx from "clsx";

import styles from "./styles.module.css";

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<"svg">>;
  description: JSX.Element;
  fill?: string;
};

const FeatureList: FeatureItem[] = [
  {
    title: "",
    Svg: require("@site/static/img/audio.svg").default,
    description: (
      <>
        <p>Check out the morning checklist for getting audio up and going.</p>
        <p className="text--center">
          <Link
            className="button button--primary button--lg"
            to="/docs/audio/morning-checklist"
          >
            Audio Checklist <ArrowRightCircle className="icon" />
          </Link>
        </p>
      </>
    ),
  },
  {
    title: "",
    Svg: require("@site/static/img/computer-svgrepo-com.svg").default,
    description: (
      <>
        <p>Check out the morning checklist for getting video up and going.</p>
        <p className="text--center">
          <Link
            className="button button--primary button--lg"
            to="/docs/video/morning-checklist"
          >
            Video Checklist <ArrowRightCircle className="icon" />
          </Link>
        </p>
      </>
    ),
  },
  {
    title: "",
    Svg: require("@site/static/img/guide.svg").default,
    description: (
      <>
        <p>Check out some of our guides to help you get started</p>
        <p className="text--center">
          <Link className="button button--primary button--lg" to="/docs/guides">
            Guides <ArrowRightCircle className="icon" />
          </Link>
        </p>
      </>
    ),
  },
];

function Feature({ title, Svg, description, fill }: FeatureItem) {
  const { colorMode } = useColorMode();
  if (colorMode === "light") {
    fill = "black"; // Battleship Gray
  } else if (colorMode === "dark") {
    fill = "white";
  }
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" fill={fill} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
