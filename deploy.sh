#! /bin/bash
set -e
export AWS_PAGER=""
yarn lint
yarn build
aws s3 sync --delete build s3://wccavdocs.beckyandzac.com
aws cloudfront create-invalidation --distribution-id E1MQZOLIIL75YO --paths "/*"
